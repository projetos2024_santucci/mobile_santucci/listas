import React  from "react"; // 
import { useRef, useState, useEffect } from "react";
import { View, PanResponder, StyleSheet, Text, Dimensions} from "react-native"

const Gesto = ({navigation}) => {
  const screenWidth = Dimensions.get('window').width;  // estamos criando uma constante para pegar a dimensão da janela, no caso, sua largura 
    const panResponder = React.useRef(
    
        PanResponder.create({
                // identifica o começo do gesto
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove:(event, gestureState) => {
                console.log('Movimento X:', gestureState.dx)
                console.log('Movimento Y:', gestureState.dy)

            },
            // identifica o final do gesto
            onPanResponderRelease:(event, gestureState) => {
                if(gestureState.dx>screenWidth/2) // verifica se passou da metade da tela
                navigation.goBack();
            }
             


        })

    ).current;


 return(

    <View style={styles.container}{...panResponder.panHandlers}>
        <Text style={styles.text}>Oi</Text>

    </View>
 );
};

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFF'
    },
    text:{
        fontSize:20,
        fontWeight:'bold'

    }


})


export default Gesto;