import React, { useState, useEffect } from 'react';
import { FlatList, View, Text, StyleSheet } from 'react-native';
import axios from 'axios';

const api = axios.create({
  baseURL: "http://10.89.234.144:5000/santucci/",
  headers: {
    'accept': 'application/json',
  },
  

});


const Users = () => {
  const [users, setUsers] = useState([]); 

  useEffect(() => {
   
    const fetchUsers = async () => {
        try {
          console.log('Iniciando requisição à API...');
          const response = await api.get('/external');
          const users = response.data.users; 
          console.log('Dados recebidos da API:', users);
          setUsers(users);
        } catch (error) {
          console.error("Error fetching users:", error);
        }
    };

    fetchUsers(); 
  }, []);



  
  async function getUsers(){
    try{
        const response = await axios.get("https://jsonplaceholder.typicode.com/users")
        setUsers(response.data);
    }
    catch(error){
        console.error("Error fetching users:", error)
    }
  }   

  getUsers();  



  return (
    <View style={styles.container}>
      <Text style={styles.textUser}>Usuários</Text>
      <FlatList
        data={i}
        keyExtractor={(i) => item.id.toString()}
        renderItem={({ i }) => (
          <View style={styles.list}>
            <Text style={styles.textContainer}>{i.name} : {i.email}</Text>
          </View>
        )}
      />
    </View>
  );
};


const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      marginTop: 35,
    },
    textUser: {
      fontSize: 24,
      fontWeight: 'bold',
      marginBottom: 5,
    },
    textContainer: {
      backgroundColor: 'purple',
      margin: 5,
      borderRadius: 8,
      fontSize: 16,
      fontWeight: '800',
      padding: 5,
      color: '#fff', // Certifique-se de que a cor contrasta com o fundo
    },
  });


export default Users;
