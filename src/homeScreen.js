import React from 'react';
import { View, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const HomeScreen = ()=>{
const navigation = useNavigation()
navigation.setOptions({
    headerTitle: 'Usuários',
    headerStyle: 
    { backgroundColor: 'black',},
    headerTintColor: '#fff',
  });
const handleUsers = () => {
    navigation.navigate("Página User")
}
const handleGestos = () => {
    navigation.navigate("Página Gestos")
}
    return(

        <View>
        <View>
            <Button title="Ver usuários" onPress={handleUsers}></Button>
        </View>
        <View>
        <Button title="Ir para aba gestos" onPress={handleGestos}></Button>
    </View>
    </View>
    )
}

export default HomeScreen;